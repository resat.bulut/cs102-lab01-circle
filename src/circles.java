
public class circles {
	public double x;
	public double y;
	public double radius;
 
	public double getX()
	{
		return x;
	}
	
	public double getY()
	{
		return y;
	}
	
	public double getRadius()
	{
		return radius;
	}
	
	public void setX(double x)
	{
		this.x = x;
	}
	
	public void setY(double y)
	{
		this.y = y;
	}
	
	public void setRadius(double radius)
	{
		this.radius = radius;
	}
	
	public double getArea()
	{
		return Math.PI * Math.pow(this.radius , 2);
	}
	
	public void translate(double dx, double dy)
	{
		this.x += dx;
		this.y += dy;
	}
	
	public double scale(double scaler)
	{
		this.radius *= scaler;
		return this.radius;
	}
	
}
